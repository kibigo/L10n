// 🌐🦁 L十n ∷ Localizations/mod.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { LazyLoader } from "../deps.js";
import LocalizationProxy from "../proxy.js";
import en from "./en.json" assert { type: "json" };

/**
 * A LazyLoader whose `strings` property gives the localization strings
 * for this module.
 *
 * Ordinarily, one would just export the strings directly, but that
 * creates a circular dependency here.
 */
export default new LazyLoader(
  Object.create(
    null,
    {
      strings: {
        configurable: false,
        enumerable: true,
        value: () =>
          new LocalizationProxy({
            en,
            [LocalizationProxy.currentLanguage]: "en",
            [LocalizationProxy.defaultLanguage]: "en",
          }),
        writable: false,
      },
    },
  ),
);
