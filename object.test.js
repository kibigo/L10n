// 🌐🦁 l十n ∷ object.test.js
// ====================================================================
//
// Copyright © 2021–2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { assertStrictEquals, describe, it } from "./dev-deps.js";
import LocalizedObject from "./object.js";
import { strings as stringsSymbol } from "./symbols.js";

const { l十n } = LocalizedObject;

describe("LocalizedObject", () => {
  describe("::l十n", () => {
    it("[[Call]] returns the current key without any binding", () => {
      assertStrictEquals(l十n`foo`, "foo");
    });

    it("[[Call]] pulls strings off of this", () => {
      assertStrictEquals(
        l十n.bind({ [stringsSymbol]: { foo: "福！" } })`foo`,
        "福！",
      );
    });

    it("[[Call]] performs substitutions", () => {
      assertStrictEquals(
        l十n.bind({ [stringsSymbol]: { sub: "$0·$1" } })`sub${"00"}`,
        "$0·00",
      );
    });

    it("[[Call]] trims A·S·C·I·I whitespace from the key", () => {
      assertStrictEquals(
        l十n.bind({ [stringsSymbol]: { _: " $1 " } })` _ ${" a "} `,
        "  a  ",
      );
    });

    it('[[Call]] drops C·G·J after "$"', () => {
      const C·G·J = String.fromCharCode(0x034F);
      assertStrictEquals(
        l十n.bind({
          [stringsSymbol]: { _: `$1$${C·G·J}2$${C·G·J}${C·G·J}3$` },
        })`_${"a"}${"b"}${"c"}`,
        `a$2$${C·G·J}3$`,
      );
    });
  });
});
