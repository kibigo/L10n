// 🌐🦁 L十n ∷ mod.test.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { assertStrictEquals } from "./dev-deps.js";
import * as L十n from "./mod.js";

const strings = new L十n.Proxy({
  en: {
    MyNamespace: {
      foo: "Foo localization",
      hello: "Hello $1",
    },
  },
  [L十n.Proxy.currentLanguage]: "en",
  [L十n.Proxy.defaultLanguage]: "en",
});

class MyNamespace extends L十n.Object {
  static get l十n() {
    return L十n.Object.l十n;
  }

  static get [L十n.strings]() {
    return strings.MyNamespace;
  }
}

Deno.test("README code is correct", () => {
  assertStrictEquals(MyNamespace.l十n`foo`, "Foo localization");
  assertStrictEquals(MyNamespace.l十n`hello ${"world"}`, "Hello world");
  assertStrictEquals(MyNamespace.l十n`bar`, "bar");
});
