// 🌐🦁 L十n ∷ Object.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import localizations from "./Localizations/mod.js";
import {
  isArraylikeObject,
  ordinaryHasInstance,
  stringReplace,
  stripLeadingAndTrailingASCIIWhitespace,
} from "./deps.js";
import { strings as stringsSymbol } from "./symbols.js";

export default class LocalizedObject {
  /**
   * The localization tag function.
   *
   * This function doesn’t have to be used as a tag, it can also be
   * called ordinarily with a key and any number of substitutions.
   */
  static l十n(keyEtCetera, ...substitutions) {
    const key = stripLeadingAndTrailingASCIIWhitespace(
      typeof keyEtCetera === "string"
        ? keyEtCetera
        : isArraylikeObject(keyEtCetera)
        ? `${keyEtCetera[0]}`
        : `${keyEtCetera}`,
    );
    const string = this?.[stringsSymbol]?.[key];
    return stringReplace(
      stringReplace(
        string == null ? key : `${string}`,
        /\$0*([1-9][0-9]*)/gu,
        (_, Ⅰ) => substitutions[+Ⅰ - 1],
      ),
      /\$\u034F/gu,
      "$",
    );
  }

  /**
   * When called on the L十n.Object constructor, returns whether the
   * provided value has the same l十n static method.
   */
  static [Symbol.hasInstance]($) {
    if (this === LocalizedObject) {
      // This is the LocalizedObject constructor; see if the provided
      // value has the same l十n static method.
      return this.l十n === $.l十n;
    } else {
      // This is not the LocalizedObject constructor.
      return ordinaryHasInstance(this, $);
    }
  }

  /** L十n.Object localization strings. */
  static get [stringsSymbol]() {
    return localizations.strings["L十n.Object"];
  }
}
Object.defineProperties(LocalizedObject, {
  name: {
    configurable: true,
    enumerable: false,
    value: "L十n.Object",
    writable: false,
  },
});
