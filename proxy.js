// 🌐🦁 L十n ∷ Proxy.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import localizations from "./Localizations/mod.js";
import {
  construct,
  defineOwnProperties,
  freeze,
  getPropertyValue,
  hasOwnProperty,
  hasProperty,
  isExtensible,
  LazyLoader,
  map,
  pop,
  reduce,
  setPropertyValue,
  toObject,
  type,
} from "./deps.js";
import LocalizationObject from "./object.js";
import { strings as stringsSymbol } from "./symbols.js";

const lazy = new LazyLoader(
  Object.create(null, {
    l十n: {
      configurable: false,
      enumerable: true,
      value: () => LocalizationObject.l十n,
      writable: false,
    },
  }),
);

/**
 * A constructor which creates a new L十n.Proxy.%Object% for
 * accessing language‐specific localization values on the provided
 * localizations object.
 *
 * The `[L十n.Proxy.currentLanguage]` and
 * `[L十n.Proxy.defaultLanguage]` properties on the provided
 * localizations object will be used (dynamically) to select the
 * starting object for property lookup. Looking up a chain of
 * properties on a localization proxy object is O(n^2) to enable
 * intermediate objects to be stored and re·used without worrying about
 * changes to the underlying localizations object. Since it is
 * anticipated n <= 3 in the common case, this is deemed acceptable.
 *
 * If you are willing to accept slightly worse errors if the property
 * you are looking for doesn’t exist, L十n.Proxy.%Object%::resolvePath
 * operates in O(n) time.
 */
const LocalizationProxy = Object.defineProperties(
  Object.setPrototypeOf(
    function (localizations) {
      if (new.target === undefined) {
        // This was not called as a constructor.
        throw new TypeError(LocalizationProxy.l十n`must construct`);
      } else {
        // This was called as a constructor.
        return constructLocalizationProxyObject(
          localizationsSymbol in localizations
            ? localizations[localizationsSymbol]
            : localizations,
        );
      }
    },
    Proxy,
  ),
  {
    /**
     * A symbol for setting the current language on a localizations
     * object.
     */
    currentLanguage: {
      configurable: false,
      enumerable: true,
      value: Symbol("L十n.Proxy.currentLanguage"),
      writable: false,
    },

    /**
     * A symbol for setting the default language on a localizations
     * object.
     *
     * This language will be used in the case of missing keys on the
     * current language.
     */
    defaultLanguage: {
      configurable: false,
      enumerable: true,
      value: Symbol("L十n.Proxy.defaultLanguage"),
      writable: false,
    },

    /**
     * A symbol for providing the underlying localizations object used
     * by a localization proxy.
     */
    localizations: {
      configurable: false,
      enumerable: true,
      value: Symbol("L十n.Proxy.localizations"),
      writable: false,
    },

    /** The localization tag function. */
    l十n: {
      configurable: true,
      enumerable: false,
      get() {
        return lazy.l十n;
      },
    },

    /** The name of the L十n.Proxy class. */
    name: {
      configurable: true,
      enumerable: false,
      value: "L十n.Proxy",
      writable: false,
    },

    /**
     * A symbol for storing the path to an intermediate localization
     * proxy object.
     */
    path: {
      configurable: false,
      enumerable: true,
      value: Symbol("L十n.Proxy.path"),
      writable: false,
    },

    /**
     * Creates a revocable localization proxy object.
     *
     * This should generally only be used in cases where the underlying
     * localizations object is intentionally ephemeral.
     */
    revocable: {
      configurable: true,
      enumerable: true,
      value: function revocable(localizations) {
        return constructLocalizationProxyObject(
          localizationsSymbol in localizations
            ? localizations[localizationsSymbol]
            : localizations,
          [],
          true,
        );
      },
      writable: true,
    },

    /** L十n.Proxy strings. */
    [stringsSymbol]: {
      configurable: true,
      enumerable: true,
      get() {
        return localizations.strings["L十n.Proxy"];
      },
    },
  },
);
export default LocalizationProxy;

/**
 * A proxy representing a partial descent into a localization object.
 *
 * This object cannot be constructed.
 */
class LocalizationProxyObject {
  /** Throws a TypeError. */
  constructor(localizations, path = [], revocable = false) {
    if (new.target !== constructLocalizationProxyObject) {
      throw new TypeError(
        LocalizationProxyObject.l十n`invalid constructor`,
      );
    } else {
      defineOwnProperties(this, {
        [localizationsSymbol]: {
          configurable: false,
          enumerable: false,
          value: toObject(localizations),
          writable: false,
        },
        [pathSymbol]: {
          configurable: false,
          enumerable: false,
          value: freeze(map(path, ($) => `${$}`)),
          writable: false,
        },
      });
      return revocable
        ? revokableProxy(this, localizationProxyHandler)
        : new ProxyConstructor(this, localizationProxyHandler);
    }
  }

  /** The localization tag function. */
  static get l十n() {
    return lazy.l十n;
  }

  /** L十n.Proxy.%Object% strings. */
  static get [stringsSymbol]() {
    return strings["L十n.Proxy.%Object%"];
  }

  /**
   * Resolves the provided path segments relative to the path location
   * of this object in its corresponding localizations object.
   *
   * This is a more efficient mechanism for resolving a path than
   * property chaining, as it skips the need to construct intermediate
   * proxies.
   */
  resolvePath(...pathSegments) {
    const path = map(pathSegments, ($) => `${$}`);
    const P = pop(path);
    const O = freeze({
      [localizationsSymbol]: toObject(this[localizationsSymbol]),
      [pathSymbol]: freeze([
        ...map(this[pathSymbol], ($) => `${$}`),
        ...path,
      ]),
    });
    return localizationProxyHandler.get(O, P, O);
  }
}
Object.defineProperty(LocalizationProxyObject, "name", {
  configurable: true,
  enumerable: false,
  value: "L十n.Proxy.%Object%",
  writable: false,
});
Object.defineProperty(
  LocalizationProxyObject.prototype,
  Symbol.toStringTag,
  {
    configurable: true,
    enumerable: false,
    value: "Localization Proxy Object",
    writable: false,
  },
);

/** The Proxy constructor. */
const ProxyConstructor = Proxy;

/**
 * Constructs a new Localization.Proxy.%Object% without throwing a
 * TypeError.
 */
const constructLocalizationProxyObject = Object.assign(
  function (localizations, path = [], revokable = false) {
    return construct(
      LocalizationProxyObject,
      [localizations, path, revokable],
      constructLocalizationProxyObject,
    );
  },
  { prototype: LocalizationProxyObject.prototype },
);

const {
  currentLanguage: currentLanguageSymbol,
  defaultLanguage: defaultLanguageSymbol,
  localizations: localizationsSymbol,
  path: pathSymbol,
} = LocalizationProxy;

const {
  /**
   * Returns the provided value unless the provided name exists on the
   * object prototype and the provided value is the same value, in
   * which case returns undefined.
   */
  ignoreObjectPrototype,
} = (() => {
  const { prototype: objectPrototype } = Object;
  return {
    ignoreObjectPrototype: (name, value) =>
      value === objectPrototype[name] ? void {} : value,
  };
})();

/** The handler used by localization proxies. */
const localizationProxyHandler = {
  get(O, P, Receiver) {
    if (typeof P !== "string" || hasOwnProperty(O, P)) {
      // P is either not a string property or exists as an own property
      // on O.
      return getPropertyValue(O, P, Receiver);
    } else {
      // P is a string property which does not exist as an own property
      // on O.
      const localizations = O[localizationsSymbol];
      const currentLanguage = localizations[currentLanguageSymbol];
      const defaultLanguage = localizations[defaultLanguageSymbol];
      const currentLanguageObject =
        currentLanguage != null && currentLanguage in localizations
          ? localizations[currentLanguage]
          : nil;
      const defaultLanguageObject =
        defaultLanguage != null && defaultLanguage in localizations
          ? localizations[defaultLanguage]
          : nil;
      const path = [...O[pathSymbol], P];
      const { result } = reduce(
        path,
        (
          { result: currentValues, defaultResult: defaultValues },
          pathComponent,
        ) => {
          if (type(currentValues) !== "object") {
            // The intermediate path result was not an object.
            throw new TypeError(
              LocalizationProxy.l十n`non·object: ${pathComponent}`,
            );
          } else {
            // The intermediate path result was an object.
            const attempt = ignoreObjectPrototype(
              pathComponent,
              currentValues[pathComponent],
            );
            const defaultResult = ignoreObjectPrototype(
              pathComponent,
              defaultValues[pathComponent],
            );
            if (attempt === undefined) {
              // P was not defined for the current language; fall back
              // to the default.
              return { result: defaultResult, defaultResult };
            } else if (defaultResult === undefined) {
              // P was defined for the current language and there was
              // no default.
              return {
                result: attempt,
                defaultResult: type(attempt) === "object"
                  ? nil
                  : undefined,
              };
            } else {
              const attemptIsObject = type(attempt) === "object";
              const defaultResultIsObject =
                type(defaultResult) === "object";
              if (
                attemptIsObject && !defaultResultIsObject ||
                !attemptIsObject && defaultResultIsObject
              ) {
                // The current language and the default have mismatching
                // types for P.
                throw new TypeError(
                  LocalizationProxy
                    .l十n`type mismatch: ${pathComponent}`,
                );
              } else {
                // Both the current language and the default have
                // defined values for P, and their types match.
                return { result: attempt, defaultResult };
              }
            }
          }
        },
        {
          result: currentLanguageObject,
          defaultResult: defaultLanguageObject,
        },
      );
      if (result === undefined) {
        // The result is undefined.
        return getPropertyValue(O, P, Receiver);
      } else if (type(result) === "object") {
        // The result is an object; it must be proxied.
        return constructLocalizationProxyObject(localizations, path);
      } else {
        // The result is not an object; it should be converted to a
        // string.
        return `${result}`;
      }
    }
  },
  has(O, P) {
    return typeof P !== "string" || hasOwnProperty(O, P)
      ? hasProperty(O, P)
      : this.get(O, P, O) !== undefined || hasProperty(O, P);
  },
  set(O, P, V, Receiver) {
    return typeof P !== "string" || hasOwnProperty(O, P)
      ? setPropertyValue(O, P, V, Receiver)
      : !isExtensible(O) || this.has(O, P)
      ? false
      : setPropertyValue(O, P, V, Receiver);
  },
};

/** An immutable empty object. */
const nil = Object.freeze(Object.create(null));

/** Returns a revokable Proxy. */
const { revokable: revokableProxy } = Proxy;
